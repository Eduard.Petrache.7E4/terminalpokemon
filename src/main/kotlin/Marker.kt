import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import io.realm.kotlin.types.ObjectId

open class Marker(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var name: String = "",
    var latitude: String = "",
    var longitude: String = "",
    var owner_id: String = "",
    var picture: ByteArray? = null,
    var type: String = ""
) : RealmObject {
    constructor() : this(owner_id = "") {}
}