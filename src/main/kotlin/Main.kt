import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

fun main() {
    val realmManager = ServiceLocator.realmManager
    var input: String

    while (true) {
        println("Benvingut a la Consola d'Administració!")
        println("Introduïu una opció:")
        println("1. Registrar-se")
        println("2. Iniciar sessió")
        println("3. Sortir")
        input = readLine() ?: ""

        when (input) {
            "1" -> {
                println("Introduïu un nom d'usuari:")
                val username = readLine() ?: ""
                println("Introduïu una contrasenya:")
                val password = readLine() ?: ""
                runBlocking {
                    realmManager.register(username, password)
                }
                adminConsole()
            }
            "2" -> {
                println("Introduïu un nom d'usuari:")
                val username = readLine() ?: ""
                println("Introduïu una contrasenya:")
                val password = readLine() ?: ""
                runBlocking {
                    realmManager.login(username, password)
                }
                adminConsole()
            }
            "3" -> {
                println("Sortint de la Consola d'Administració...")
                break
            }
            else -> {
                println("Opció invàlida, si us plau intenti-ho de nou.")
            }
        }
    }
}

fun adminConsole() {
    val markersDao = ServiceLocator.markersDao
    var input: String

    while (true) {
        println("\nConsola d'Administració")
        println("Introduïu una opció:")
        println("1. Llistar marcadors")
        println("2. Afegir marcador")
        println("3. Actualitzar nom del marcador")
        println("4. Eliminar marcador")
        println("5. Actualitzar tipus de marcador")
        println("6. Tancar sessió")
        input = readLine() ?: ""

        when (input) {
            "1" -> {
                runBlocking {
                    val markers = markersDao.listFlow().first()
                    markers.forEach { marker ->
                        println("ID: ${marker._id}, Nom: ${marker.name}, Latitud: ${marker.latitude}, Longitud: ${marker.longitude}, Tipus: ${marker.type}")
                    }
                }
            }

            "2" -> {
                println("Introduïu el nom del marcador:")
                val name = readLine() ?: ""
                println("Introduïu la latitud:")
                val latitude = readLine() ?: ""
                println("Introduïu la longitud:")
                val longitude = readLine() ?: ""
                runBlocking {
                    markersDao.insertItem(name, latitude, longitude)
                }
            }

            "3" -> {
                println("Introduïu l'ID del marcador a actualitzar:")
                val markerId = readLine() ?: ""
                println("Introduïu el nou nom del marcador:")
                val newTitle = readLine() ?: ""
                runBlocking {
                    markersDao.updateTitle(markerId, newTitle)
                }
            }

            "4" -> {
                println("Introduïu l'ID del marcador a eliminar:")
                val markerId = readLine() ?: ""
                runBlocking {
                    markersDao.deleteItem(markerId)
                }
            }

            "5" -> {
                println("Introduïu l'ID del marcador a actualitzar:")
                val markerId = readLine() ?: ""
                println("Introduïu el nou tipus del marcador:")
                val newType = readLine() ?: ""
                runBlocking {
                    markersDao.updateMarkerType(markerId, newType)
                }
            }

            "6" -> {
                println("Tancant sessió...")
                break
            }

            else -> {
                println("Opció invàlida, si us plau intenti-ho de nou.")
            }
        }
    }
}

