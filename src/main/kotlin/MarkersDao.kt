import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.mongodb.kbson.ObjectId


class MarkersDao(val realm: Realm, val userID: String) {
    fun listFlow(): Flow<List<Marker>> =
        realm.query<Marker>("owner_id == $0", userID).find().asFlow().map {
            it.list.toList()
        }


    fun insertItem(text: String, latitude: String, longitude: String){
        realm.writeBlocking {
            val marker = Marker(name = text, latitude = latitude, longitude = longitude, owner_id = userID )
            copyToRealm(marker)
        }
    }

    fun updateTitle(markerId: String, newTitle: String) {
        realm.writeBlocking {
            val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
            marker.name = newTitle
        }
    }

    suspend fun deleteItem(markerId: String) {
        withContext(Dispatchers.IO) {
            realm.write {
                val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
                delete(marker)
            }
        }
    }

    fun updateMarkerType(markerId: String, newType: String) {
        realm.writeBlocking {
            val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
            marker.type = newType
        }
    }
}